package main

import (
	"os"
	"testing"
)

func TestParseData(t *testing.T) {

	file, err := os.Open("currencies.json")
	if err != nil {
		t.Errorf("error opening file, %s", err.Error())
	}
	currencies, err := parseCurrencies(file)

	if err != nil {
		t.Errorf("Error parsing currencies from file: %s", err.Error())
	}

	if len(currencies.Listings) != 168 {
		t.Errorf("Expected 168 currencies, got %d", len(currencies.Listings))
	}

	for _, v := range currencies.Listings {
		if v.ID == "" {
			t.Error("got id empty")
		}
	}

}
