# Creating a http-server

Creating a http-server is simple and straight forward. This is the complete hello world http-server in go, batteries included:    

    package main
    
    import (
    	"log"
    	"net/http"
    )
    
    func main() {
    	http.HandleFunc("/", handleFunc)
    	log.Fatal(http.ListenAndServe(":8080", nil))
    }
    
    func handleFunc(writer http.ResponseWriter, _ *http.Request) {
    	writer.Write([]byte("hello http"))
    }


Each request will spawn a goroutine that lives until the request is finished or cancelled.

`http.HandleFunc` takes a pattern-string and a handler-function as an argument. This function registers the handler-function on the http-handler. 

`http.ListenAndServe` will block until an error is returned or the program is shut down

# Assignment 

1. Try to run the hello-http program locally. 
2. Rewrite your application to a server that writes the json-string of all the buy-prices for your chosen cryptocurrency.
3. Congrats. You have now created a highly concurrent web-server that can handle thousands of request per second. 
 
 
 [next: goroutines](../06_GoRoutines/GoRoutines.md)
