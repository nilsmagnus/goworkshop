# Go workshop

This workshop is intended for people with some experience in programming. The content of this workshop is 
designed to give the participants a taste of what the go-programming language is and some of its special features. 

This is not an exhaustive workshop in go and does not cover all aspects of the language or the tooling. 

Advanced usage of the topics mentioned in this workshop are left out. Parts that relates to methods, reflection, packages, dependencies are left out completely. 

For a complete guide to the language, refer to the book [The Go Programming Language](https://www.oreilly.com/library/view/the-go-programming/9780134190570/) by Brian W. Kernighan and Alan A. A. Donovan. 

# Prerequisites

- Programming skills in at least one imperative language and some knowledge of http.
- A computer with access-rights to install software.
- Installation of go and set up GOPATH
- Add $GOPATH/bin to your $PATH
- An editor compatible with text-files, preferrably with code highlighting

## Installation

* Install go https://golang.org/dl/ (__mac__: brew install golang)
    *  check your installation `go version`. If it prints some sane version of go, you are set.
* [Set up your editor](/00_setup/Setup.md). If your editor is not listed, please roll your own and make sure
go fmt is run when you want it to(probably as often as possible). Go imports should be set up as well, and consider 
running tests on each save. 
* install goimports with `go get golang.org/x/tools/cmd/goimports`
* __IntelliJ-users__: dont clone this repo with git, use _go get_: `go get gitlab.com/nilsmagnus/goworkshop` . 

# Outline

.00. [Editor setup](/00_setup/Setup.md). Editor setup for vs-code and intellij.

1. [Hello types](/01_hello/HelloTypes.md). Hello world and introduction to the types in go.
2. [Control structures](/02_control/ControlStructures.md). For, if, intro to http and err.
3. [Structs and json](/03_Structs/Structs.md). Structs, pointers and json.Unmarshal
4. [Functions and testing](/04_Functions/Functions.md). Testing our structs and marshalling functions.
5. [Http server](/05_HttpServer/HttpServer.md). Create a server and serve the buy-prices you got from 4 or the currencies from 5.
6. [Go routines](/06_GoRoutines/GoRoutines.md). `go` keyword, sync.Waitgroup, (possibly channels and orchestrations)
7. [Deployment to aws or gcp](/07_Faas/Faas.md). Deploy your app to appengine or aws. 
