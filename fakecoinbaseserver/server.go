package main

import (
	"encoding/json"
	"fmt"
	"io/ioutil"
	"log"
	"net/http"
	"os"
)

func main() {

	http.HandleFunc("/", func(writer http.ResponseWriter, request *http.Request) {
		writer.Write([]byte("/v2/currencies and /v2/prices/:crypto-:actual/buy is supported"))
	})

	http.HandleFunc("/v2/currencies", currenciesFromJson())

	for _, v := range buypairsFromJson() {
		for _, base := range []string{"BTC", "LTC", "ETH", "BCH"} {
			pair := fmt.Sprintf("%s-%s", base, v.Data.Currency)
			v.Data.Base = base
			http.HandleFunc(fmt.Sprintf("/v2/prices/%s/buy", pair), buypairFunction(v))
		}
	}

	log.Fatal(http.ListenAndServe(":8080", nil))
}
func buypairFunction(price BuyPrice) func(http.ResponseWriter, *http.Request) {
	return func(w http.ResponseWriter, r *http.Request) {
		jsonBytes, jerr := json.Marshal(&price)
		if jerr != nil {
			panic(jerr)
		}
		w.Write(jsonBytes)
	}
}

func buypairsFromJson() map[string]BuyPrice {
	file, ferr := os.Open("buypairs.json")
	if ferr != nil {
		panic(ferr)
	}
	bytes, rerr := ioutil.ReadAll(file)
	if rerr != nil {
		panic(ferr)
	}

	buypairs := map[string]BuyPrice{}
	json.Unmarshal(bytes, &buypairs)

	return buypairs
}

func currenciesFromJson() func(http.ResponseWriter, *http.Request) {
	file, ferr := os.Open("currencies.json")
	if ferr != nil {
		panic(ferr)
	}
	bytes, rerr := ioutil.ReadAll(file)
	if rerr != nil {
		panic(ferr)
	}
	currencies := CoinBaseCurrencies{}
	json.Unmarshal(bytes, &currencies)
	jsonBytes, jerr := json.Marshal(&currencies)
	if jerr != nil {
		panic(jerr)
	}
	return func(w http.ResponseWriter, r *http.Request) {
		w.Write(jsonBytes)
	}
}

type CoinBaseCurrencies struct {
	Listings []Currency `json:"data"`
}

type Currency struct {
	ID      string
	Name    string `json:"name"`
	MinSize string `json:"min_size"`
}

type BuyPrice struct {
	Data struct {
		Base     string `json:"base"`
		Currency string `json:"currency"`
		Amount   string `json:"amount"`
	} `json:"data"`
}
