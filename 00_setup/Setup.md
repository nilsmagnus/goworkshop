Recommendation of editor: the one you are used to. Vs-code has more features than the intellij-plugin, but its niceties rather than must-haves.


# Intellij 

Install the "Go" and "File watchers" plugins from the jetbrains repository and restart. 

Go to Preferences->Tools->File Watchers and press "+" to add the templates "go fmt" and "goimports". Save them as project-wide watchers if you dont want them run on all your intellij-projects.

# VS code

Install the go plugin and reload. The editor will spend some time installing some useful tools, wait until finished.

Formatting is enabled by default. 

If you want to, you can have coverage on save:
Go to Preferences->Settings->Extensions->Go Configuration. Check "Cover on save" 

# Others

There is about 2 million go-programmers out there(oct. 2018) with their own taste and opinions. A lot of people
use vi and emacs, so it should be fairly easy to find setup for these editors. 


[next:hello types](../01_hello/HelloTypes.md)
