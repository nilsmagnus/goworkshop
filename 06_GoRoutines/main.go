package main

import (
	"encoding/json"
	"fmt"
	"io"
	"io/ioutil"
	"log"
	"net/http"
)

func main() {
	http.HandleFunc("/", handleFunc)
	log.Fatal(http.ListenAndServe(":8080", nil))
}

func handleFunc(writer http.ResponseWriter, _ *http.Request) {
	if prices, err := fetchBuyPrices(); err != nil {
		writer.Write([]byte("failed to fetch prices: " + err.Error()))
	} else {
		jsonBytes, err := json.Marshal(&prices)
		if err != nil {
			writer.Write([]byte("failed to marshal prices: " + err.Error()))
		} else {
			writer.Write(jsonBytes)
		}
	}
}

type CoinBaseCurrencies struct {
	Listings []Currency `json:"data"`
}

type Currency struct {
	ID      string `json:"id"`
	Name    string `json:"name"`
	MinSize string `json:"min_size"`
}

type BuyPrice struct {
	Data struct {
		Base     string `json:"base"`
		Currency string `json:"currency"`
		Amount   string `json:"amount"`
	} `json:"data"`
}

func fetchBuyPrices() (map[string]BuyPrice, error) {
	cs, err := getCurrencies("https://kodemakercoinbaseapi.appspot.com/v2/currencies")
	if err != nil {
		return nil, err
	}

	prices := map[string]BuyPrice{}
	for _, v := range cs.Listings {
		buyPair := fmt.Sprintf("%s-%s", "BTC", v.ID)
		apiUrl := fmt.Sprintf("https://kodemakercoinbaseapi.appspot.com/v2/prices/%s/buy", buyPair)
		if price, err := getBuyPriceFor(apiUrl); err != nil {
			fmt.Printf("Got error for pair %s: %s", buyPair, err.Error())
		} else {
			fmt.Printf("got result for pair %s \n", buyPair)
			prices[buyPair] = price
		}
	}
	return prices, nil
}

func getCurrencies(s string) (CoinBaseCurrencies, error) {
	if val, err := http.Get(s); err != nil {
		return CoinBaseCurrencies{}, err
	} else {
		defer val.Body.Close()
		if cs, err := parseCurrencies(val.Body); err != nil {
			return CoinBaseCurrencies{}, err
		} else {
			return cs, nil
		}
	}
}

func getBuyPriceFor(apiUrl string) (BuyPrice, error) {
	if val, err := http.Get(apiUrl); err != nil {
		return BuyPrice{}, err
	} else {
		defer val.Body.Close()
		return parseBuyPrice(val.Body)
	}

}

func parseBuyPrice(reader io.Reader) (BuyPrice, error) {
	result := BuyPrice{}
	if bytes, err := ioutil.ReadAll(reader); err != nil {
		return result, err
	} else {
		if jsonErr := json.Unmarshal(bytes, &result); jsonErr != nil {
			return result, err
		}
		return result, nil
	}
}

func parseCurrencies(reader io.Reader) (CoinBaseCurrencies, error) {
	result := CoinBaseCurrencies{}
	if bytes, err := ioutil.ReadAll(reader); err != nil {
		return result, err
	} else {
		if jsonErr := json.Unmarshal(bytes, &result); jsonErr != nil {
			return result, err
		}
		return result, nil
	}
}
