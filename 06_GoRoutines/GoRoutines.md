# Goroutines

To do things concurrently in go, goroutines are used. They can be seen as lightweight threads that are not scheduled
by the operating system, but is scheduled by the go-runtime itself. They allocate a stack of 4Kb memory initially and does not have
any upper limit on the stack-size. These features makes them suitable for both short running small tasks and tasks that requires 
a lot of memory and computation. 

To launch a function in a new go-routine, use the `go` keyword. 

    go somefunc() // "somefunc" is running in a separate goroutine
     

Sometimes simply launching a goroutine without any callback or reporting is ok as a fire-and-forget operation, but usually we
want to use the result of the goroutine to continue some other computation at some point. To orchestrate this and synchronize 
goroutines there are some special built-in tools. 

These includes:

- the channel type, to communicate between goroutines. This is a thread-safe queue designed for the purpose of communicating between goroutines.
- waitgroups, mutexes, thread-safe maps and other structs in the `sync` package

More advanced locks like rw-locks and semaphores are also provided.

# Assignment 1

1. Take the contents of the for loop where you fetch buy-prices and run it in a local anonymous function that takes a currency as input:

from 

    for _, v := range... {
        /* contents */
    }
    
to 

    for _, v := range...{
        go func(currency Currency){
            /* contents */
        }(v)
    }

What happens when we run this?

Hint: in the go func, dont use the loop-variable v directly, use a copy or the passed variable to the anonymous function.

## Waitgroups
Waitgroups are a nice struct built for dealing with concurrency.

    wg := sync.WaitGroup{} // create a waitgroup
    
    wg.Add(1) // add one counter to the waitgroup
    
    wg.Done() // count down 1 for the waitgroup
    
    wg.Wait() // wait for the waitgroup to become 0
   
## Assignment 2
1. Create a waitgroup before the for-loop
2. For each loop step, add 1 to the waitgroup outside the goroutine
3. Inside the goroutine, use the defer-statement to count down the waitgroup
4. After the for loop, use wg.Wait to wait for all results

Run the program several times. Are the results consistent?
5. Run the program with the `-race` flag: `go run -race main.go` and do a request. What is the output?
    
## Locks

Locks makes sure that only 1 process is running a block of code at a time. 

    m := sync.Mutex{}
    
    m.Lock()
    // update shared structures
    m.Unlock()

## Assignment 3

1. Add a lock to the program and lock/unlock around the update of the map. 
2. Run the program again with the `-race` flag: `go run -race main.go`. Now, how are the results this time?

## Channels

As we have seen, communicating between goroutines using shared variables causes a lot of pain with synchronization of shared variables. Therefore we have channels.
 
There is a famous _go-proverb_ that sums up why we have channels:

_"Don't communicate by sharing memory, share memory by communicating."_ [-Rob Pike, gopherfest 2015](https://www.youtube.com/watch?reload=9&v=PAAkCSZUG1c&t=2m48s).


If we share memory between goroutines by sharing variables and passing pointers to structs, there is a good chance that 
we run into some concurrency-problems or deadlocks, not knowing who updates what and when. The solution to this is simply not sharing memory with shared
variables to communicate between goroutines. 


To create a channel we have to use the `make` keyword with the type of the channel and optional capacity. Capacity is how many values 
a channel can have at one point. If a channel is full, it will block the go-routine when sending to it. A read will block until there 
are values to read or the channel is closed. 

    channel := make(chan int, 10) // create a channel for ints with capacity 10
    

The channel type is a thread-safe type that serves as a communication-link between goroutines. 

Writing and reading to/from a channel is done with "arrow" syntax:

    channel <- 1  // send the int 1 to channel
    
    a:= <- channel // wait for a result on channel and assign it to a
    
    
Or, if we want to read a stream of values from a channel, we can use the `range` keyword:

    for value := range channel {
        // do something with value
    }

This will try to read values from channel until the channel is closed. Closing the channel is done with the built-in function `close()`


    close(channel)


## Done-channels and value-channels and go-routines


Often we want to have a _value-channel_ and a _meta-channel_ to orchestrate go-routines. It is not always a go-routine is able 
to post a value to the channel due to an error or other circumstances, so we introduce a done-channel consisting only of boolen value. 
Posting to the done channel is done in a defer-statement as the first thing a go-routine does so that we know when a goroutine is finished or not:


    package main
    
    import (
    	"fmt"
    	"math/rand"
    	"time"
    )
    
    func main() {
    	rand.Seed(time.Now().UnixNano())
    
    	doneChannel := make(chan bool, 10) // done-channel
    	values := make(chan int, 10)       // value-channel
    
    	onDone := func() { close(values) } // when all dones are accounted for, we want to run this
    
    	// create 10000 goroutines that sends a value
    	routineCount := 10000
    	for i := 0; i < routineCount; i++ {
    		go produceAValue(doneChannel, values, i)
    	}
    
    	go callOnDoneCount(doneChannel, onDone, routineCount)
    
    	for v := range values {
    		fmt.Printf("%d ", v) // print the results from all the channels until closed
    	}
    
    	fmt.Println("\n values channel closed, we are done")
    }
    
    func produceAValue(doneChan chan bool, intChannel chan int, valueToSend int) {
    	defer func() { doneChan <- true }() // always send to done when goroutine is done
    	if rand.Float32() > 0.5 {           // sometimes send value, sometimes not
    		intChannel <- valueToSend // write an int to channel
    	}
    }
    
    func callOnDoneCount(done chan bool, onDone func(), routineCount int) {
    	doneCount := 0
    	for range done {
    		doneCount++
    		if doneCount == routineCount {
    			onDone()
    		}
    	}
    }

    
# Final Assignment

1. Rewrite your previous application from [section 05](../05_HttpServer/HttpServer.md) or the previous assignment to use goroutines to fetch buy-prices.


[next(optional): faas](../07_Faas/Faas.md)
