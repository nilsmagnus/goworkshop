package main

import (
	"encoding/json"
	"fmt"
	"io"
	"io/ioutil"
	"net/http"
	"sync"
)

func main() {
	cs, err := getCurrencies("https://api.coinbase.com/v2/currencies")
	if err != nil {
		panic(err)
	}

	prices := map[string]BuyPrice{}
	m := &sync.Mutex{}
	wg := sync.WaitGroup{}
	for _, currency := range cs.Listings {
		wg.Add(1)
		go func(v Currency) {
			defer wg.Done()
			buyPair := fmt.Sprintf("%s-%s", "BTC", v.ID)
			apiUrl := fmt.Sprintf("https://api.coinbase.com/v2/prices/%s/buy", buyPair)

			if price, err := getBuyPriceFor(apiUrl); err != nil {
				fmt.Printf("Got error for pair %s: %s", buyPair, err.Error())
			} else {
				m.Lock()
				prices[buyPair] = price
				m.Unlock()
			}
		}(currency)
	}
	wg.Wait()
	fmt.Printf("Length of prices are %d: \n %v", len(prices), prices)
}

func getCurrencies(s string) (CoinBaseCurrencies, error) {
	if val, err := http.Get(s); err != nil {
		return CoinBaseCurrencies{}, err
	} else {
		defer val.Body.Close()
		if cs, err := parseCurrencies(val.Body); err != nil {
			return CoinBaseCurrencies{}, err
		} else {
			return cs, nil
		}
	}
}

func getBuyPriceFor(apiUrl string) (BuyPrice, error) {
	if val, err := http.Get(apiUrl); err != nil {
		return BuyPrice{}, err
	} else {
		defer val.Body.Close()
		return parseBuyPrice(val.Body)
	}

}

func parseBuyPrice(reader io.Reader) (BuyPrice, error) {
	result := BuyPrice{}
	if bytes, err := ioutil.ReadAll(reader); err != nil {
		return result, err
	} else {
		if jsonErr := json.Unmarshal(bytes, &result); jsonErr != nil {
			return result, err
		}
		return result, nil
	}
}

func parseCurrencies(reader io.Reader) (CoinBaseCurrencies, error) {
	result := CoinBaseCurrencies{}
	if bytes, err := ioutil.ReadAll(reader); err != nil {
		return result, err
	} else {
		if jsonErr := json.Unmarshal(bytes, &result); jsonErr != nil {
			return result, err
		}
		return result, nil
	}
}

type CoinBaseCurrencies struct {
	Listings []Currency `json:"data"`
}

type Currency struct {
	ID      string `json:"id"`
	Name    string `json:"name"`
	MinSize string `json:"min_size"`
}

type BuyPrice struct {
	Data struct {
		Base     string `json:"base"`
		Currency string `json:"currency"`
		Amount   string `json:"amount"`
	} `json:"data"`
}
