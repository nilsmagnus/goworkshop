package main

import (
	"encoding/json"
	"fmt"
	"io"
	"io/ioutil"
	"log"
	"net/http"
)

func main() {
	http.HandleFunc("/", handleRequests)
	log.Fatal(http.ListenAndServe(":8080", nil))
}

func handleRequests(writer http.ResponseWriter, _ *http.Request) {

	cs, err := getCurrencies("https://kodemakercoinbaseapi.appspot.com//v2/currencies")
	if err != nil {
		panic(err)
	}

	channel := make(chan BuyPrice, 10)
	done := make(chan bool, 10)

	for _, v := range cs.Listings {
		go getPrice(v, channel, done)
	}

	fmt.Println("len is "  , len(cs.Listings))
	doneCount := 0

	prices := map[string]BuyPrice{}
	for {
		select {
		case <-done:
			doneCount ++
			if doneCount == len(cs.Listings) {
				fmt.Printf("Length of prices are %d: \n %v", len(prices), prices)
				responseBytes, _ := json.Marshal(&prices)
				writer.Write(responseBytes)
				return
			}
		case v := <-channel:
			key := fmt.Sprintf("%s-%s", v.Data.Base, v.Data.Currency)
			prices[key] = v
		}
	}
}

func getPrice(v Currency, channel chan BuyPrice, done chan bool) {
	defer func() { done <- true }()
	buyPair := fmt.Sprintf("%s-%s", "BTC", v.ID)
	apiUrl := fmt.Sprintf("https://kodemakercoinbaseapi.appspot.com/v2/prices/%s/buy", buyPair)

	if price, err := getBuyPriceFor(apiUrl); err != nil {
		fmt.Printf("Got error for pair %s: %s", buyPair, err.Error())
	} else {
		channel <- price
	}
}

func getCurrencies(s string) (CoinBaseCurrencies, error) {
	if val, err := http.Get(s); err != nil {
		return CoinBaseCurrencies{}, err
	} else {
		defer val.Body.Close()
		if cs, err := parseCurrencies(val.Body); err != nil {
			return CoinBaseCurrencies{}, err
		} else {
			return cs, nil
		}
	}
}

func getBuyPriceFor(apiUrl string) (BuyPrice, error) {
	if val, err := http.Get(apiUrl); err != nil {
		return BuyPrice{}, err
	} else {
		defer val.Body.Close()
		return parseBuyPrice(val.Body)
	}

}

func parseBuyPrice(reader io.Reader) (BuyPrice, error) {
	result := BuyPrice{}
	if bytes, err := ioutil.ReadAll(reader); err != nil {
		return result, err
	} else {
		if jsonErr := json.Unmarshal(bytes, &result); jsonErr != nil {
			return result, err
		}
		return result, nil
	}
}

func parseCurrencies(reader io.Reader) (CoinBaseCurrencies, error) {
	result := CoinBaseCurrencies{}
	if bytes, err := ioutil.ReadAll(reader); err != nil {
		return result, err
	} else {
		if jsonErr := json.Unmarshal(bytes, &result); jsonErr != nil {
			return result, err
		}
		return result, nil
	}
}

type CoinBaseCurrencies struct {
	Listings []Currency `json:"data"`
}

type Currency struct {
	ID      string `json:"id"`
	Name    string `json:"name"`
	MinSize string `json:"min_size"`
}

type BuyPrice struct {
	Data struct {
		Base     string `json:"base"`
		Currency string `json:"currency"`
		Amount   string `json:"amount"`
	} `json:"data"`
}
