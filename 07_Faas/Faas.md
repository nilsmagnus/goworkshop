# Deployment

This has little to do with the golang, and can be considered an extra excercise if you want 
to get to know AWS lambda or Google Appengine. 

The simplest way is to go with the Google App Engine solution as we dont have to modify our code and there is only 1 line of configuration for your app. 

Setting up AWS takes some more steps and you have to depend on some special aws-library to include in your code to make it run. 

## Deploying to AWS

This guide is not exhaustive and probably contains misleading information.

Set up your account according to the README in [https://github.com/kodemaker/skoddemaker](https://github.com/kodemaker/skoddemaker) .

The guides on https://github.com/aws/aws-lambda-go and https://docs.aws.amazon.com/lambda/latest/dg/lambda-go-how-to-create-deployment-package.html should be a good start.

From what I can see you have to :

0. set up your aws account(1 time config) and install the aws-cli
1. create a iam-role for your lambda
2. modify your app to include the aws library(get the library with `$ go get github.com/aws/aws-lambda-go/lambda`)
3. cross-compile your server to a binary and zip it
4. upload the zip-file 
5. Have a beer. Repeat step 5. 

## Deploying to Google App Engine

Install gcloud cli from https://cloud.google.com/sdk/install

Go to https://console.cloud.google.com and create a project. 

    $ gcloud components install app-engine-go
    # installation takes a minute or two
    
    $ gcloud auth login
    # a browser should pop up and you select an account
    
    $ gcloud config set project YOUR_PROJECTID_GOES_HERE
    
    $ gcloud app deploy // config file "app.yaml" is in this folder    
    # select Y or press enter to deploy. Wait 20-40 seconds on first deployment.
    
You have now deployed your wonderful currency-app, have a beer. 