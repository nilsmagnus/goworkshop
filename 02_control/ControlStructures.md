# Loops

Loops come with 1 and only 1 keyword: `for`, but it comes in several variants. 

The most basic variant is 

     for initialization; condition; post {
        // zero or more statements
     }

The `while` loop is expressed as

     for condition {
        // ...
     } 

Where condition can be omitted to create an infinite loop

    for {
        //
    }
    
To do something useful with the contents of an array, slice, channel or map, the `range` keyword is used

    for index , value := range slice {
        // do something with each index/value
    }


Looping over contents of a map is very similar :

    for key, value := range yourMap {
        // do something with each key/value    
    }

# Conditions

Conditions in go are a bit different than many other languages. The structure is as follows;

    if <statement>; condition {
        // condition was true, vars created in statement are in scope
    } else {
        // condition was false, vars created in statement are still in scope
    }
    
    
    if value, err := somefunc(); err != nil {
       // handle err
    } else {
       // do something with value, value is still in scope
    }

If we dont need a statement, we can omit it and only use the condition-part

    if aBooleanExpression {
        // do it
    }
    
   


# Switch (for completeness)
 
Switch is simple and brute, no exhaustive matching is required. There is no fallthrough

    switch os := runtime.GOOS; os {
    	case "darwin":
    		fmt.Println("OS X.")
    	case "linux":
    		fmt.Println("Linux.")
    	default:
    		// freebsd, openbsd,
    		// plan9, windows...
    		fmt.Printf("%s.", os)
    }
    
The open switch:

    switch{
        case x<0:
            
        case y>1:
    }    
    	
(Code is copied directly from [part 9 of "A Tour of Go"](https://tour.golang.org/flowcontrol/9).)

# Defer

The `defer` statements takes a function as argument and runs it after the current function is completed. Much like
 a finally-clause, except there is no try-catch in go. This is often used to close resources. The deferred statement
 will execute no matter what happens in the rest of the function.

    func deferTest(){
        defer fmt.Println("I am after the function completed")
        fmt.Println("i am in the function")
    }

# Errors and (ignoring)returned values

`error` is a special built-in interface in go. It is returned from functions which can fail in some way or another. 
It is good practice to check the content of error instead of ignoring it, but if we are pretty sure that a function will
not fail, we can ignore the returned error by assigning it to the special character `_`, which means "i don't care about this returned value".

    a, _ := func()(int,int){return 1,2}() // a will get the value 1, while the returned value '2' is discarded

Unused variables causes compilation errors, so assigning to unused vars to `_` is the solution if you don't use all returned variables. 

Unused imports will also cause compilation errors, so if you want to import a package just for its side-effects, you can import it with `_`:

    import _ "image/png" // will register the png-decoder and make sure it gets compiled into your binary/library
 


# Assignment

1. Modify your helloworld by importing the `net/http` package and invoke `http.Get()` to fetch the content of "https://api.coinbase.com/v2/currencies". Check if the returned error has a value and print the value of the error if it is non-nil. 
If there is no error, use the `ioutil.ReadAll()` function from the package `io/ioutil`to read the value from the `Body`-field of the http-response. Discard the error returned from the `ReadAll` function by assigning it to `_`. Print the results to stdout. 
2. Upset the program by trying an invalid url and see what `err` contains.
3. Body has a Close method. Close it using the defer-statement, before ready anything from it.

- Hint 1: to convert a byte-slice to string, cast it with `string(byteslice)`.
- Hint 2: we will probably be throttled by the coinbase-api and evicted for an hour or two due to too many requests from 1 ip-address. Use "https://kodemakercoinbaseapi.appspot.com/v2/currencies" for url if that happens.

[next: structs](../03_Structs/Structs.md)
