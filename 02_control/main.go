package main

import (
	"fmt"
	"os"
)

func main() {
	fmt.Printf("%v\n", os.Args)

	slice := append(os.Args, "how", "you", "doin", "there", "?")

	fmt.Printf("%v\n", slice[len(slice)-4:len(slice)-2])


	spreaded := append(slice, slice...)

	fmt.Printf("spread : %v\n", spreaded)
}
