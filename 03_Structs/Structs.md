# Struct

The struct type is a composite type composed of of other types. The fields of a struct can be any type. 

    type CoinBaseCurrencies struct {
    	Listings []Currency `json:"data"`
    }
    
    type Currency struct {
    	ID      string `json:"id"`
    	Name    string `json:"name"`
    	Min     string  
    }

A struct that contains only comparable types is also comparable(you can compare two structs with ==). If one or more fields of
the struct is non-comparable, the struct will become non-comparable. 

The zero-value of a struct is a struct with all the fields with their zero-values. 

## Initializing structs

    currencies := CoinBaseCurrencies{} // create an empty struct with zero-values of the fields

	// use names for the fields you want to set
	sek := Currency{
		ID: "NOK",
		Name : "Krone",
	}

	// without naming the fields, we have set all fields in order
	nok := Currency{"nok", "krone", "0.111000"}

	currencies.Listings = []Currency{sek,nok}

Above are some suggestions for structs for the json results that were returned from the coinbase api. 

## Pointers
In go, we can choose between sending values or sending references when passing parameters. 

When passing a "raw" struct to a function, a copy of the struct is made and passed to the function. 
To avoid allocating unnecessary memory a _pointer_ to a struct 
is often used to increase performance.  This allows the receiver of the pointer 
to modify the data the pointer is pointing to, so take caution when passing pointers and reference-types as parameters.

A pointer to an existing struct is made by giving it an `&`:

    nok := Currency{}
    pointerToNok := &nok

To retrieve the value of the pointer , use *

    var sek Currency = *pointerToNok // make a copy of nok into the variable sek

Since this is a bit confusing, the compiler helps us a bit so that we don't need the star when operating on it:

    symbol := pointerToNok.ID
    
    pointerToNok.ID = "eur" // beware, we just changed nok
  

## Field tags 

In the suggestion for structs for coinbasecurrencies above, some of the fields are tagged with some json-meta info. Such tags can be used for anything
and can be read using the reflection-api. They are typically used when marshalling and unmarshalling structs to xml, json, protobuf, avro etc. You are free to create your own tags if you want to. 

# Json 

Go provides a library for working with json. The package  `encoding/json` provides the central functions "Marshal" and "Unmarshal". The `Unmarshall()` function takes a byte-slice and a pointer to an existing struct and returns an error. It will populate the struct with the contents of the byte-array.


    func Unmarshal(data []byte, v interface{}) error {/**/}
    
To see the full documentation on the function, run the command `go doc encoding/json Unmarshal`

# Assignment
1. Instead of just printing the result from coinbase, Unmarshal it into the struct above. 
2. The struct above is missing a tag on the field Min, complete them with tags.
3. Loop over all the currencies and print their id, symbol and name. 
3. Now, use the function `json.Marshal()` to re-create the json-string from the struct and print it to the console with fmt.Println. Note that Marshal() should take a pointer as argument.


As you might notice, error handling can be quite intrusive in go if we handle all errors. Feel free to ignore the errors with `_` when you feel it is safe. 

The error handling is one of the top complaints in the go community, it is expected to improve in later versions of go.

[next: functions](../04_Functions/Functions.md)