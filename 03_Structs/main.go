package main

import (
	"fmt"
	"io/ioutil"
	"net/http"
)

func main() {
	if val, err := http.Get("https://kodemakercoinbaseapi.appspot.com/v2/currencies"); err != nil {
		fmt.Printf("Got error, %v", err)
	} else {
		defer val.Body.Close()
		result, _ := ioutil.ReadAll(val.Body)
		fmt.Println(string(result))
	}
}
