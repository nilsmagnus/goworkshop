package main

import (
	"encoding/json"
	"fmt"
	"io/ioutil"
	"net/http"
)

type CoinBaseCurrencies struct {
	Listings []Currency `json:"data"`
}

type Currency struct {
	ID      string `json:"id"`
	Name    string `json:"name"`
	MinSize string `json:"min_size"`
}

func main() {
	if val, err := http.Get("https://api.coinbase.com/v2/currencies"); err != nil {
		fmt.Printf("Got error, %v", err)
	} else {
		defer val.Body.Close()
		result, _ := ioutil.ReadAll(val.Body)
		cs := CoinBaseCurrencies{}
		if jsonErr := json.Unmarshal(result, &cs); jsonErr != nil {
			panic(jsonErr)
		} else {
			jsonBytes, _ := json.Marshal(cs)
			fmt.Println(string(jsonBytes))
		}
	}
}
