# Functions

Functions are first class citizens in go. 

## Functions vs methods

Both really mean the same thing: a callable sub-program within a larger program. However, methods tend to be used about operations directly coupled
with an object or data-structure in OO languages, where we cannot call methods without constructing the objects. 

In go we have both methods and functions. I will only show functions in this workshop. Is go an OO language? YMMV. It depends on your view and chosen programming style.

## Variable scoping and functions

Functions inherit the scope of the context they are defined in. 

    var b = "global b"
    var phantom = 0.1

    func main(){
        something := "a box"
        phantom := 1.2 // redeclaration of a variable in parent scope, aka shadowing
        fun := func (){
            c := "inner"
            fmt.Println(something, "is available here, along with ", b, "while phantom ",phantom,"is shadowed", c)
        }
        
        // c is not available here

        fun()
    }
    
    // no fun() here

    
This is called `closure` and is one of the reasons why functions are not comparable. Clousures enable the possibility
of memoization and caching among other things. 

## Parameters and return types

Functions declare the name and type of their parameters. Return types can be declared as types or as named variables. 
We can return as many values we want to from a function, there is no need to create our own "Pair, Tuple, Tripes"-types to return 
more than one value from a function. 

If we name the return variables or we have 2 or more variables are returned, we need to wrap the return-types in parentheses.

    // unnamed returns
    func someFunc( parameter1 typename, parameter2, parameter3 typeof2and3) returntype {/* function body*/}
    
    // more than 1 return-type needs to be wrapped in parantheses
    func nameOfFunction( parameter1 typename, parameter2, parameter3 typeof2and3) (returntype, returntype2) {/* function body*/}


Named returns initializes the variables for us, but are harder to read since we dont explicitly say what is returned

    // named returns     
    func a()(result int){
        return // "result" is returned. 
    }


Named returns can be confusing and should probably be avoided.


## Functions are types

Since functions are treated as just as any other types in go, we can pass them as function parameters and return them from functions. 

Consider we want to calculate the 13 first fibonacci-numbers, closures and functions can come to use:


    type producer func() int  // type producer is a func that returns int
    
    func sequence(p producer, size int) []int { // accept function as parameter
    	result := []int{}
    	for i := 0; i < size; i++ {
    		result = append(result, p())
    	}
    	return result
    }
    
    func fibonacciProducer() producer { // return fibonacci-producer
    	x, y, z := 0, 1, 0
    	return func() int {
    		z, x, y = x, y, x+y
    		return z
    	}
    }
    
    func main() {
    	fib := fibonacciProducer()
    
    	fmt.Printf("%T \n", fib)
    
    	fmt.Printf("%v", sequence(fib, 13)) // pass the fibonacci-function as a parameter, prints [0 1 1 2 3 5 8 13 21 34 55 89 144]
    }

## Testing (functions)


Tests are simple as everything else in go. They come in 2 flavours; the normal test and the benchmark-test.

Tests are placed in files that end with `_test.go` in the same folder as the production code you are testing. 
The compiler will make sure it is not a part of your compiled binary. If you want to test `main.go`, the corresponding testfile 
should be called `main_test.go` and placed in the same folder as `main.go` and be a part of the same package.

Test-functions takes in a pointer to testing.T, returns nothing. The function-name must start with "Test" to be executed. 


    func TestWhateverYouAreTesting(t *testing.T){ // testing.T is the "normal" test. testing.B is a benchmark test
        // your test
        if something != expected {
            t.Error("something failed, got ", something) // t.Error records an error and continues the test
            t.Fatal(" cannot continue") // t.Fatal stops the test right here
        }
    }


Tests are run with the command `go test`. Intellij-users must set $GOPATH to include the project they are testing(Settings->Languages And Frameworks->Go->GOPATH). Command-line execution and execution
from VS-code works out of the box. 

# Assignment
1. http.Response.Body implements the `io.Reader` interface. Create a function that takes a 
io.Reader as parameter and returns a struct with the parsed data from coinbase from the 
[previous chapter](../03_Structs/Structs.md). 
2. Create a test for the function you just created. Since the function takes io.Reader as 
parameter, we can test the function with a file with json-contents we expect to get from the api. 
Here is a raw file with sample response: [currencies.json](currencies.json). `os.Open()` is a nice function to use to open the file in go.
3. Extract the whole function for getting the prices into a new function that takes an url as parameter and returns `(CoinBaseCurrencies,error)`. 
4. There is another coinbase api that will give you the buy price for the crypto-currencies supported by coinbase(BTC, LTC and ETH) for all the supported currencies. This api is 

`GET https://api.coinbase.com/v2/prices/:currency_pair/buy`

Where we substitute `:currency_pair` with <crypto-symbol>-<currency-symbol>, e.g BTC-USD:

`curl -X GET -v https://api.coinbase.com/v2/prices/BTC-USD/buy`

5. Create a new struct for the result-json from the api and write for loop for all currencies to get the buy-price for your favourite crypto-currency(BTC,ETH or LTC). Store the results in a map[string]YourNewStruct, where the key is the "buy-pair" (e.g "BTC-USD").
6. Print the end-result with `fmt.Printf("Got  %d prices : \n%v", len(prices), prices)`


Hint: if you are getting error-messages from coinbase-api, its because we are spamming the api from a single ip. Use https://kodemakercoinbaseapi.appspot.com/ as server if this happens. This server is serving static content. 

## extra assignments
1. Write a test for the fibonacci function. 
2. The fibonacci function returns different results every time. Can you modify it to take a number,n, as parameter and make the function return the first `n` numbers in the fibonacci sequence instead? Make sure you cache the result so that the calculations are not repeated. 


[next: httpserver](../05_HttpServer/HttpServer.md)
