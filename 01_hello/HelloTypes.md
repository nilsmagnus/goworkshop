# Hello world


    package main  
    
    import "fmt"
    
    func main(){
        fmt.Println("Hello संसार") 
    }
    
* __package main__ means that we probably have a main method we want to run as an application.
* __import "fmt"__ means that we are importing the fmt-package from the core-library. If we import multiple packages, group them in parantheses.
* __fmt.Println()__ is a function provided by the fmt package that prints to stdout. 

One of the go-creators also created UTF-8, so unicode compatibility is a no-brainer. All go source code 
should be saved as UTF-8.

### Building a go program

    # compile and run without producing a binary
    $ go run <mainfile.go>

    # compile all packages and produce binary
    $ go build

    # compile and put in $GOROOT/bin
    $ go install
    
    # run tests
    $ go test
    
    # show documentation 
    $ go doc packagename [FunctionName]
    
 These are the basic commands. There are flags for cross-compiling, running special set of tests, specifying number of cores to use etc. 

# Assignment 

1. Create your hello-world app and run it with `go run`. 
2. Locate the docs for the function fmt.Println using either `go doc fmt Println` or look it up on [https://godoc.org/-/go](https://godoc.org/-/go)

# Types

Here are the types in go and their default values. Basic types defaults to a real value and 
reference-types defaults to `nil` if they are not initialized. Types marked with * are types that differs substantially
in behaviour from other languages. 

|Type | nil/default | comparable| * | assignment|
|-----|-------------|-----------|---|---| 
|int , int8,int16,int32,int64| 0| yes| |a :=0|
|uint , uint8,uint16,uint32,uint64| 0| yes| |a :=0|
|float32, float64| 0.0| yes| | a:= 0.0|
|complex64, complex128| (0.0,0.0i)| yes|*| a := complex(5,7)|
| string| ""| yes| | a:= "hei" | 
|rune (alias for int32) | 0| yes|(*)| a := 'a' |
|bool | false| yes| | a := true |
|array| array of defaults| yes| | a := [1]int{14}
|struct| default of its fields| depends| (*) | a := Person{ age:0 }|
|slice| nil | no| *| a := []{}|
|map| nil | no| | a := map[string]string{}|
|func| nil | no| | a := func(){}|
|chan| nil | no| (*)| a := make(chan int)|
|pointers| nil | no| (*)| a: = &b|
|interface| nil | no| *| implicitly inferred |

Reference types are typically nil by default and are not comparable. Reference types are types that refers to a value or a set of values, typically a map, slice or a pointer. 
Comparable in this case means that a variable can be checked to be equal to another variable of same type. Channels, pointers, maps and slices are not comparable because they cannot be used as keys in a map since their size and content may change over time. Functions are not comparable because they might have enclosing variables, making the impossible to compare to eachother.
Arrays are only comparable if they are arrays of comparable types and the length is the same.

## Type alias

Create a type aliases to make your own types. 

    type Foo int8 // type Foo is comparable only to another Foo
    
## Casting 

If types are of the same base type (numeric or custom types), they can be cast with parentheses:

    var b Foo = 2
    a := int(b)
   

## Variable assignment

There are 3(4) ways to assign variables:


    var a int = 0 //  declare a variable, its name, type and value
    var b int  //  same as above, but to the default zero-value
    var c = 0 // declare a variable, its name and value, type is inferred
    d := 0 // declare a variable name and its value. Type is inferred. note the ':=' operator
    
The far most common assignment method is the latter one, but you will see the first one when we want to be specific about a type( e.g. int8 or int16 instead of int ).

We can assign multiple variables in one line

    a,b := 1,2
    
    // go crazy, assign 4 variables in one line!
    a, b, z, d := 0 , 1, "hey", 1+1
 
Creating a new variable copies the content of the first variable, they don´t share memory-content

    a := 1
    b := a // b is now a copy of a, they dont share data
    
    a++ // b is unaffected

    fmt.Printf("%d > %d", a,b) // prints 2 > 1   
  

# Slices

Arrays demand that you specify their size upfront and is brittle to work with. Slices are look a lot like arrays, but has some important differences. 
It is the equivalent to lists in other languages and are a bit more flexible than arrays. 

Slices are __"a view to an underlying array"__ and we cannot add or remove elements from it. To expand a slice, we create a new slice with values of an existing slice. Re-assigning to an existing slice is perfectly legal.

Creating slices

    a := []int{} // slice literal for empty slice
    a := make([]int,initialcapacity) // create a slice with an initial capacity
    a := []int{1,2,4} // a slice with initial values
   

Slice metadata:

    a := []int{1,2,3}
    fmt.Printf("lenght is %d, capacity is %d \n", len(a), cap(a))
    
`len()` and `cap()` are built-in functions. `len` will give you the length of the slice, `cap` will give you the capacity of the underlying array.
    
    // nil-slices
    var b []int   // a nil slice
    len(b)  // is 0
    cap(b) // is 0
    fail := b[1] // panic, out of range
    c := append(b,1) // ok

Appending to a slice:

    b := append(a,19) // we are not really appending to the slice a, we are creating a new slice b with one more element


Getting the `n` first elements of a slice:

    b := a[:n]

Getting all elements after `m`:

    c := a[m:]

# Maps

Maps are created with `make` or a map-literal

    a := make(map[string]string)
    b := map[string]string{"foo":"bar", "baz":"qux"}

    a["somethjing"] = "anythjing"
    
    // get value for a key in map
    value, ok := a["doesntexist"]  // value == "" , ok == false
    
    // we dont need to know if the value exists, we can omit the boolean
    value := a["maybeExist"]
    
    // we just want to know if the value exists, we need to ignore the value-assignment with "_"
    
    _, ok := a["checkexistence"]

Take note of how to ignore a returned variable with **_** (underscore).

Value and ok are to different types assigned in the same statement. 

# The missing set-type

There is no set-type in go. Any suggestions on why?

    aBunchOfStringKeys := map[string]bool 

# Package fmt 

We can use the package "fmt" to print and format strings. The function `fmt.Printf()` formats strings and prints to stdout. There are a number of formatting directives
that can format your values in a string. `%d` formats an int, `%T` prints the type of the variable

     fmt.Printf("%T %v", b, b ) // will print the type of b and the int-value of b
     
Commonly used formatting directives:

|directive | type|
|---|---|
| %d| int type|
| %f| float type|
| %v| value of anything|
| %T| type of a variable|
| %#v| readable string-representation of any var|

# Code formatting

There is exactly 1 way to correctly format your code, and that is done with the command `gofmt`. Most editors has "save actions" that can run this command to make 
sure your code is always well formatted.


# Assignment

1. There is an "os" package. `os.Args` is a slice that contains the command line arguments. Import the "os" package to your hello world and print the contents of `os.Args`.
2. Compile the program and run it with some arguments. Dont print the first argument(the executable name).
3. Create a new slice with os.Args as the starting slice and append some value(s) to  it. What is the type of the slice? How is the capacity and length evolving? 
4. Create and print a slice with the elements between 5 and 7 of the slice `a := []interface{}{9,"a",0.0,func(){},0,9,42,1337, 'a'}`.
5. Print the 3 last elements of the same slice.
6. Create a third slice that appends `os.Args` to `os.Args` , run from command-line with some custom arguments. 

Hints: `append()` takes a vararg. And slices has the spread-operator `...`


[next:control structures](../02_control/ControlStructures.md)
